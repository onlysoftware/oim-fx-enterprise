package com.oim.core.business.box;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.oim.core.bean.Department;
import com.oim.core.bean.DepartmentMember;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractBox;

/**
 * @author: XiaHui
 * @date: 2016年9月28日 下午3:18:16
 */
public class DepartmentBox extends AbstractBox {

	public DepartmentBox(AppContext appContext) {
		super(appContext);
	}

	/**
	 * 组织结构相关的数据
	 */

	/***** 所有部门 ******/
	private Map<String, Department> departmentMap = new ConcurrentHashMap<String, Department>();
//	private Map<String, List<String>> departmentMap = new ConcurrentHashMap<String, List<String>>();
//	private Map<String, List<String>> departmentMap = new ConcurrentHashMap<String, List<String>>();
	
	/*****
	 * 部门真实成员（不含子部门）<key:departmentId(部门id),value:Map<key:userId(用户id),value>>
	 ***/
	private Map<String, Map<String, DepartmentMember>> departmentMemberListMap = new ConcurrentHashMap<String, Map<String, DepartmentMember>>();
	/*****
	 * 用户所在部门（不含上级部门）<key:userId(用户id),value:Map<key:departmentId(部门id),value>>
	 *****/
	private Map<String, Map<String, DepartmentMember>> userInDepartmentMemberListMap = new ConcurrentHashMap<String, Map<String, DepartmentMember>>();

	/**
	 * 部门群数据
	 */
	/**** 部门成员(包含子部门)<key:departmentId(部门id),value:List<userId(用户id)>> ***/
	private Map<String, List<String>> departmentMemberIdListMap = new ConcurrentHashMap<String, List<String>>();
	/**** 用户所在部门(包含上级部门)<key:departmentId(部门id),value:List<userId(用户id)>> ***/
	private Map<String, List<String>> userInDepartmentIdListMap = new ConcurrentHashMap<String, List<String>>();

	private List<DepartmentMember> departmentMemberList;
	private List<Department> departmentList;

	private void putDepartment(Department department) {
		departmentMap.put(department.getId(), department);
	}

	private void putDepartmentMember(DepartmentMember departmentMember) {
		Map<String, DepartmentMember> departmentMemberMap = getDepartmentMemberMap(departmentMember.getDepartmentId());
		departmentMemberMap.put(departmentMember.getUserId(), departmentMember);

		Map<String, DepartmentMember> userInDepartmentMemberMap = getUserInDepartmentMemberMap(departmentMember.getUserId());
		userInDepartmentMemberMap.put(departmentMember.getDepartmentId(), departmentMember);
	}

	public void setDepartmentMemberList(List<DepartmentMember> departmentMemberList) {
		this.departmentMemberList = departmentMemberList;
		if (null != departmentMemberList) {
			for (DepartmentMember departmentMember : departmentMemberList) {
				putDepartmentMember(departmentMember);
			}
		}
		loadDepartmentMemberList(departmentList, departmentMemberList);
	}

	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
		if (null != departmentList) {
			for (Department department : departmentList) {
				putDepartment(department);
			}
		}
		loadDepartmentMemberList(departmentList, departmentMemberList);
	}

	private void loadDepartmentMemberList(List<Department> departmentList, List<DepartmentMember> departmentMemberList) {
		departmentMemberIdListMap.clear();
		userInDepartmentIdListMap.clear();
		if (null != departmentList && null != departmentMemberList) {

			Map<String, Set<String>> departmentMemberIdSetMap = new HashMap<String, Set<String>>();// 所有部门成员<部门id，用户id集合>

			for (DepartmentMember dm : departmentMemberList) {
				String departmentId = dm.getDepartmentId();
				String userId = dm.getUserId();
				Set<String> memberIdSet = departmentMemberIdSetMap.get(departmentId);
				if (memberIdSet == null) {
					memberIdSet = new HashSet<String>();
					departmentMemberIdSetMap.put(departmentId, memberIdSet);
				}
				memberIdSet.add(userId);
			}

			Map<String, Set<Department>> nodeMap = new HashMap<String, Set<Department>>();
			Map<String, Set<Department>> superMap = new HashMap<String, Set<Department>>();

			Map<String, Department> departmentMap = new HashMap<String, Department>();
			for (Department d : departmentList) {
				departmentMap.put(d.getId(), d);
			}

			for (Department d : departmentList) {// 循环找出上级部门
				String id = d.getId();
				Set<Department> superSet = superMap.get(id);
				if (null == superSet) {
					superSet = new HashSet<Department>();
					superMap.put(id, superSet);
				}
				String superiorId = d.getSuperiorId();

				Department superior = departmentMap.get(superiorId);
				Set<Department> tempSet = null;
				while (null != superior) {

					superSet.add(superior);

					Set<Department> nodeList = nodeMap.get(superiorId);
					if (nodeList == null) {
						nodeList = new HashSet<Department>();
						nodeMap.put(superiorId, nodeList);
					}
					nodeList.add(d);
					if (null != tempSet) {
						nodeList.addAll(tempSet);
					}

					tempSet = nodeList;
					d = superior;
					superiorId = superior.getSuperiorId();
					superior = departmentMap.get(superiorId);
				}
			}

			for (Department d : departmentList) {
				String id = d.getId();
				Set<String> memberIdSet = departmentMemberIdSetMap.get(id);
				if (memberIdSet == null) {
					memberIdSet = new HashSet<String>();
					departmentMemberIdSetMap.put(id, memberIdSet);
				}

				Set<Department> set = nodeMap.get(id);

				if (null != set) {
					for (Department n : set) {
						Set<String> nodeMemberIdSet = departmentMemberIdSetMap.get(n.getId());
						if (null != nodeMemberIdSet) {
							memberIdSet.addAll(nodeMemberIdSet);
						}
					}
				}
			}

			for (Department d : departmentList) {
				String id = d.getId();
				List<String> idList = new ArrayList<String>();
				Set<String> memberIdSet = departmentMemberIdSetMap.get(id);
				if (memberIdSet != null) {
					idList.addAll(memberIdSet);
				}
				departmentMemberIdListMap.put(id, idList);
			}
			for (DepartmentMember dm : departmentMemberList) {
				String departmentId = dm.getDepartmentId();
				String userId = dm.getUserId();

				List<String> departmentIdList = userInDepartmentIdListMap.get(userId);
				if (departmentIdList == null) {
					departmentIdList = new ArrayList<String>();
					userInDepartmentIdListMap.put(userId, departmentIdList);
				}

				departmentIdList.add(departmentId);

				Set<Department> departmentSet = superMap.get(departmentId);
				if (departmentSet != null) {
					for (Department d : departmentSet) {
						String id = d.getId();
						if (!departmentIdList.contains(id)) {
							departmentIdList.add(id);
						}
					}
				}
			}
		}
	}

	/**
	 * 获取用户所在的所有部门
	 * 
	 * @param userId
	 * @return
	 */
	public Map<String, DepartmentMember> getUserInDepartmentMemberMap(String userId) {
		Map<String, DepartmentMember> departmentMemberMap = userInDepartmentMemberListMap.get(userId);
		if (null == departmentMemberMap) {
			departmentMemberMap = new HashMap<String, DepartmentMember>();
			userInDepartmentMemberListMap.put(userId, departmentMemberMap);
		}
		return departmentMemberMap;
	}

	/**
	 * 获取用户所在的所有部门
	 * 
	 * @param userId
	 * @return
	 */
	public List<DepartmentMember> getUserInDepartmentMemberList(String userId) {
		Map<String, DepartmentMember> departmentMemberMap = getUserInDepartmentMemberMap(userId);
		List<DepartmentMember> list = new ArrayList<DepartmentMember>();
		if (null != departmentMemberMap) {
			list.addAll(departmentMemberMap.values());
		}
		return list;
	}

	/**
	 * 获取部门的成员
	 * 
	 * @param departmentId
	 * @return
	 */
	public List<DepartmentMember> getDepartmentMemberList(String departmentId) {
		Map<String, DepartmentMember> departmentMemberMap = departmentMemberListMap.get(departmentId);
		List<DepartmentMember> list = new ArrayList<DepartmentMember>();
		if (null != departmentMemberMap) {
			list.addAll(departmentMemberMap.values());
		}
		return list;
	}

	/**
	 * 获取部门的成员
	 * 
	 * @param departmentId
	 * @return
	 */
	public Map<String, DepartmentMember> getDepartmentMemberMap(String departmentId) {
		Map<String, DepartmentMember> departmentMemberMap = departmentMemberListMap.get(departmentId);
		if (null == departmentMemberMap) {
			departmentMemberMap = new HashMap<String, DepartmentMember>();
			departmentMemberListMap.put(departmentId, departmentMemberMap);
		}
		return departmentMemberMap;
	}

	public Department getDepartment(String id) {
		return departmentMap.get(id);
	}

	/**
	 * 获取所有部门
	 * 
	 * @return
	 */
	public List<Department> getDepartmentList() {
		return new ArrayList<Department>(departmentMap.values());
	}

	/**
	 * 获取部门用户数量
	 * 
	 * @param departmentId
	 * @return
	 */
	public int getDepartmentMemberSize(String departmentId) {
		Map<String, DepartmentMember> departmentMemberMap = departmentMemberListMap.get(departmentId);
		return null == departmentMemberMap ? 0 : departmentMemberMap.size();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 用户是否在部门中包含上级部门
	 * 
	 * @param userId
	 * @param departmentId
	 * @return
	 */
	public boolean inDepartmentIncludeSuperior(String userId, String departmentId) {
		boolean in = false;
		List<String> departmentIdList = userInDepartmentIdListMap.get(userId);
		if (null != departmentIdList) {
			in = departmentIdList.contains(departmentId);
		}
		return in;
	}

	/**
	 * 获取用户所在部门idList包含上级部门
	 * 
	 * @author XiaHui
	 * @date 2017年8月22日 下午4:05:58
	 * @param userId
	 * @return
	 */
	public List<String> getDepartmentIdListcludeSuperior(String userId) {
		List<String> departmentIdList = userInDepartmentIdListMap.getOrDefault(userId, new ArrayList<String>());
		return departmentIdList;
	}

	/**
	 * 获取用户的数量，包含子部门
	 * 
	 * @param departmentId
	 * @return
	 */
	public int getDepartmentMemberSizeIncludeNode(String departmentId) {
		List<String> list = departmentMemberIdListMap.get(departmentId);
		return null == list ? 0 : list.size();
	}

	/**
	 * 获取部门成员id，包含子部门
	 * 
	 * @param departmentId
	 * @return
	 */
	public List<String> getDepartmentMemberIdListIncludeNode(String departmentId) {
		List<String> list = departmentMemberIdListMap.getOrDefault(departmentId, new ArrayList<String>());
		return list;
	}
}
