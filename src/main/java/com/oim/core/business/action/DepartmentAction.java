package com.oim.core.business.action;

import java.util.List;

import com.oim.core.bean.Department;
import com.oim.core.bean.DepartmentMember;
import com.oim.core.business.sender.DepartmentSender;
import com.oim.core.business.service.DepartmentService;
import com.oim.core.business.service.HeadService;
import com.only.general.annotation.action.ActionMapping;
import com.only.general.annotation.action.MethodMapping;
import com.only.general.annotation.parameter.Define;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractAction;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.bean.UserHead;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年6月14日 下午9:31:55
 * @version 0.0.1
 */

@ActionMapping(value = "1.1100")
public class DepartmentAction extends AbstractAction {

	public DepartmentAction(AppContext appContext) {
		super(appContext);
	}

	@MethodMapping(value = "1.1.0001")
	public void setUserDataList(
			@Define("userDataList") List<UserData> userDataList) {
		DepartmentService ds = appContext.getService(DepartmentService.class);
		ds.setUserDataList(userDataList);
	}

	@MethodMapping(value = "1.1.0002")
	public void setDepartmentList(
			@Define("departmentList") List<Department> departmentList) {
		DepartmentService ds = appContext.getService(DepartmentService.class);
		ds.setDepartmentList(departmentList);
	}

	@MethodMapping(value = "1.1.0003")
	public void setDepartmentMemberList(
			@Define("departmentMemberList") List<DepartmentMember> departmentMemberList) {
		DepartmentService ds = appContext.getService(DepartmentService.class);
		ds.setDepartmentMemberList(departmentMemberList);
	}

	/***
	 * 接受服务器传来好友列表信息
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 */
	@MethodMapping(value = "1.1.0004")
	public void setDepartmentWithUserList(
			@Define("departmentList") List<Department> departmentList,
			@Define("userDataList") List<UserData> userDataList,
			@Define("departmentMemberList") List<DepartmentMember> departmentMemberList) {
		DepartmentService ds = appContext.getService(DepartmentService.class);
		ds.setDepartmentWithUserList(departmentList, userDataList, departmentMemberList);
	}

	@MethodMapping(value = "1.1.0005")
	public void setUserHeadList(@Define("headList") List<UserHead> headList) {
		HeadService headService = appContext.getService(HeadService.class);
		headService.setUserHeadList(headList);
	}

	@MethodMapping(value = "1.1.0007")
	public void setDepartmentWithUserList(
			@Define("department") Department department) {
		DepartmentService ds = appContext.getService(DepartmentService.class);
		ds.setDepartment(department);
	}

	// /***
	// * 接受用户信息更新
	// *
	// * @Author: XiaHui
	// * @Date: 2016年2月16日
	// * @ModifyUser: XiaHui
	// * @ModifyDate: 2016年2月16日
	// */
	// @MethodMapping(value = "1.2.0008")
	// public void updateUserStatus(@Define("userId") String userId,
	// @Define("status") String status) {
	// UserBox ub=appContext.getBox(UserBox.class);
	// UserData userData = ub.getUserData(userId);
	// if (null != userData) {
	// userData.setStatus(status);
	// UserService userService = appContext.getService(UserService.class);
	// userService.updateUserData(userData);
	// }
	// }

	@MethodMapping(value = "1.2.1001")
	public void updateDepartmentList() {
		DepartmentSender ds = appContext.getSender(DepartmentSender.class);
		ds.getDepartmentList();
	}

	@MethodMapping(value = "1.2.1002")
	public void updateDepartment(@Define("departmentId") String departmentId) {
		DepartmentSender ds = appContext.getSender(DepartmentSender.class);
		ds.getDepartment(departmentId);
	}

	@MethodMapping(value = "1.2.1003")
	public void updateDepartmentMemberList() {
		DepartmentSender ds = appContext.getSender(DepartmentSender.class);
		ds.getUserList();
		ds.getDepartmentMemberList();
	}
}
