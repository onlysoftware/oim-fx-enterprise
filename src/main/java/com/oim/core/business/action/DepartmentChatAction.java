package com.oim.core.business.action;

import com.oim.core.business.service.DepartmentChatService;
import com.only.general.annotation.action.ActionMapping;
import com.only.general.annotation.action.MethodMapping;
import com.only.general.annotation.parameter.Define;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractAction;
import com.onlyxiahui.im.message.data.chat.Content;

/**
 * 描述：负责接受聊天相关业务的控制层
 * 
 * @author 夏辉
 * @date 2014年6月14日 下午9:31:55
 * @version 0.0.1
 */

@ActionMapping(value = "1.500")
public class DepartmentChatAction extends AbstractAction {

	public DepartmentChatAction(AppContext appContext) {
		super(appContext);
	}

	@MethodMapping(value = "1.2.5001")
	public void receiveDepartmentChatMessage(
			@Define("userId") String userId,
			@Define("departmentId") String departmentId,
			@Define("content") Content content) {
		DepartmentChatService chatService=appContext.getService(DepartmentChatService.class);
		chatService.receiveDepartmentChatMessage(userId, departmentId,content);
	}
}
