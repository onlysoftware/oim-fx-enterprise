package com.oim.core.business.manage;

import com.oim.core.bean.Department;
import com.oim.fx.view.EnterpriseMainView;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractManager;

/**
 * 描述：对主界面的记录列表的管理
 * 
 * @author XiaHui
 * @date 2015年4月12日 上午10:18:18
 * @version 0.0.1
 */
public class DepartmentLastManage extends AbstractManager {

	public DepartmentLastManage(AppContext appContext) {
		super(appContext);
	}

	public void addOrUpdateLastDepartment(Department department, String text) {
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.addOrUpdateLastDepartment(department, text);
	}

}
