package com.oim.core.business.manage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oim.core.bean.Department;
import com.oim.core.business.manager.PromptManager;
import com.oim.fx.view.EnterpriseChatListView;
import com.oim.fx.view.EnterpriseMainView;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractManager;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.message.data.chat.Content;

/**
 * 对聊天相关的一些管理，如不同用户聊天界面
 * 
 * @author XiaHui
 * @date 2015年3月16日 下午1:37:57
 */
public class DepartmentChatManage extends AbstractManager {

	
	Map<String, List<DepartmentChatData>> departmentChatMap = new HashMap<String, List<DepartmentChatData>>();
	
	public DepartmentChatManage(AppContext appContext) {
		super(appContext);
		initEvent();
	}

	private void initEvent() {
	}


	public void putDepartmentCaht(String departmentId,UserData userData,Content content) {
		List<DepartmentChatData> list = departmentChatMap.get(departmentId);
		if (list==null) {
			list=new ArrayList<DepartmentChatData>();
			departmentChatMap.put(departmentId, list);
		}
		list.add(new DepartmentChatData(userData,content));
	}
	
	public boolean isDepartmentChatShowing(String departmentId) {
		EnterpriseChatListView chatListView = appContext.getSingleView(EnterpriseChatListView.class);
		return chatListView.isDepartmentChatShowing(departmentId);
	}


	public void departmentChat(Department department, UserData userData, Content content) {
		EnterpriseChatListView chatListView = appContext.getSingleView(EnterpriseChatListView.class);
		chatListView.departmentChat(department, userData, content);
	}


	public void showCahtFrame(Department department) {
		EnterpriseChatListView chatListView = appContext.getSingleView(EnterpriseChatListView.class);
		chatListView.show(department);
		chatListView.setVisible(true);
		
		PromptManager pm = this.appContext.getManager(PromptManager.class);
		pm.remove(department.getId());// 系统托盘停止跳动
		
		EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
		mainView.showDepartmentHeadPulse(department.getId(),  false);// 停止头像跳动
	}
	

	public void showDepartmentCaht(Department department) {
		List<DepartmentChatData> list = departmentChatMap.remove(department.getId());
		DepartmentChatManage chatManage = this.appContext.getManager(DepartmentChatManage.class);
		if (null != department && list != null && !list.isEmpty()) {
			for (DepartmentChatData ucd : list) {
				chatManage.departmentChat(department, ucd.userData, ucd.content);
			}
		}
	}
	class DepartmentChatData{
		
		private UserData userData;
		private Content content;
		
		public DepartmentChatData(UserData userData,Content content){
			this.userData = userData;
			this.content = content;
		}
		public UserData getUserData() {
			return userData;
		}
		public void setUserData(UserData userData) {
			this.userData = userData;
		}
		public Content getContent() {
			return content;
		}
		public void setContent(Content content) {
			this.content = content;
		}
	}

	public void updateDepartmentUserList(String departmentId) {
		EnterpriseChatListView chatListView = appContext.getSingleView(EnterpriseChatListView.class);
		chatListView.updateDepartmentUserList(departmentId);
	}
}
