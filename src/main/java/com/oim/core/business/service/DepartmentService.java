package com.oim.core.business.service;

import java.util.List;

import com.oim.core.bean.Department;
import com.oim.core.bean.DepartmentMember;
import com.oim.core.business.manage.DepartmentManage;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractService;
import com.onlyxiahui.im.bean.UserData;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年3月31日 上午11:45:15 version 0.0.1
 */
public class DepartmentService extends AbstractService {

	public DepartmentService(AppContext appContext) {
		super(appContext);
	}

	public void setDepartmentWithUserList(List<Department> departmentList, List<UserData> userDataList, List<DepartmentMember> departmentMemberList) {
		DepartmentManage dm = this.appContext.getManager(DepartmentManage.class);
		dm.setDepartmentWithUserList(departmentList, userDataList, departmentMemberList);
	}

	public void setDepartment(Department department) {
		DepartmentManage dm = this.appContext.getManager(DepartmentManage.class);
		dm.addOrUpdateDepartment(department);
	}

	public void setUserDataList(List<UserData> userDataList) {
		DepartmentManage dm = this.appContext.getManager(DepartmentManage.class);
		dm.setUserDataList(userDataList);
	}

	public void setDepartmentList(List<Department> departmentList) {
		DepartmentManage dm = this.appContext.getManager(DepartmentManage.class);
		dm.setDepartmentList(departmentList);
	}

	public void setDepartmentMemberList(List<DepartmentMember> departmentMemberList) {
		DepartmentManage dm = this.appContext.getManager(DepartmentManage.class);
		dm.setDepartmentMemberList(departmentMemberList);
	}
}
