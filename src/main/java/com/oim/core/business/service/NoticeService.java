package com.oim.core.business.service;

import com.oim.core.business.view.NoticeView;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractService;
import com.onlyxiahui.im.message.data.notice.TextNotice;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年3月31日 上午11:45:15 version 0.0.1
 */
public class NoticeService extends AbstractService {

	public NoticeService(AppContext appContext) {
		super(appContext);
	}

	public void showNotice(TextNotice textNotice) {

		if (null != textNotice) {
			NoticeView nv = this.appContext.getSingleView(NoticeView.class);

			String openType = textNotice.getOpenType();// 1:app 2：browser
			String url = textNotice.getUrl();
			String title = textNotice.getTitle();
			String content = textNotice.getContent();
			nv.addTextNotice(openType, url, title, content);
		}
	}
}
