package com.oim.core.business.service;

import com.oim.common.chat.util.CoreContentUtil;
import com.oim.core.bean.Department;
import com.oim.core.business.box.DepartmentBox;
import com.oim.core.business.box.UserDataBox;
import com.oim.core.business.manage.DepartmentChatManage;
import com.oim.core.business.manage.DepartmentLastManage;
import com.oim.core.business.manager.PromptManager;
import com.oim.core.business.sender.UserSender;
import com.oim.core.common.action.CallAction;
import com.oim.core.common.app.dto.PromptData.IconType;
import com.oim.core.common.sound.SoundHandler;
import com.oim.fx.view.EnterpriseMainView;
import com.only.general.annotation.parameter.Define;
import com.only.net.action.Back;
import com.only.net.data.action.DataBackAction;
import com.only.net.data.action.DataBackActionAdapter;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractService;
import com.onlyxiahui.im.bean.UserData;
import com.onlyxiahui.im.message.data.chat.Content;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年3月31日 上午11:45:15 version 0.0.1
 */
public class DepartmentChatService extends AbstractService {

	public DepartmentChatService(AppContext appContext) {
		super(appContext);
	}

	public void receiveDepartmentChatMessage(String userId, String departmentId, Content content) {
		DepartmentBox db=appContext.getBox(DepartmentBox.class);
		Department department = db.getDepartment(departmentId);
		if (null != department) {
			UserDataBox ub=appContext.getBox(UserDataBox.class);
			UserData userData = ub.getUserData(userId);
			if (null == userData) {
				DataBackAction dataBackAction = new DataBackActionAdapter() {
					@Back
					public void back(@Define("userData") UserData userData) {
						ub.putUserData(userData);
						showChatData(department, userData, content);
					}
				};
				UserSender uh = this.appContext.getSender(UserSender.class);
				uh.getUserDataById(userId, dataBackAction);
			} else {
				showChatData(department, userData, content);
			}
		}
	}

	
	private void showChatData(Department department, UserData userData, Content content) {
		DepartmentChatManage chatManage = this.appContext.getManager(DepartmentChatManage.class);
		if (!chatManage.isDepartmentChatShowing(department.getId())) {

			CallAction callAction = new CallAction() {

				@Override
				public void execute() {
					DepartmentChatManage chatManage = appContext.getManager(DepartmentChatManage.class);
					chatManage.showCahtFrame(department);
				}
			};
			chatManage.putDepartmentCaht(department.getId(), userData, content);
			PromptManager pm = this.appContext.getManager(PromptManager.class);
			String headPath = "Resources/Images/Enterprise/Head/2.png";
			pm.put(department.getId(), IconType.pathImage, headPath, callAction);
			
			pm.playSound(SoundHandler.sound_type_message);
			
			EnterpriseMainView mainView = this.appContext.getSingleView(EnterpriseMainView.class);
			mainView.showDepartmentHeadPulse(department.getId(), true);
		} else {
			chatManage.departmentChat(department, userData, content);
		}
		String text = CoreContentUtil.getText(content);
		DepartmentLastManage lastManage = this.appContext.getManager(DepartmentLastManage.class);
		lastManage.addOrUpdateLastDepartment(department, text);
	}
}
