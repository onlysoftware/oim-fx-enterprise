package com.oim.core.cover.action;

import java.util.HashMap;
import java.util.List;

import com.oim.core.business.box.UserDataBox;
import com.oim.core.cover.service.EnterpriseUserService;
import com.only.general.annotation.action.ActionMapping;
import com.only.general.annotation.action.MethodMapping;
import com.only.general.annotation.parameter.Define;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.app.base.component.AbstractAction;
import com.onlyxiahui.im.bean.UserData;

/**
 * 描述：
 * 
 * @author 夏辉
 * @date 2014年6月14日 下午9:31:55
 * @version 0.0.1
 */

@ActionMapping(value = "1.101")
public class EnterpriseUserAction extends AbstractAction {

	public EnterpriseUserAction(AppContext appContext) {
		super(appContext);
	}

	
	/***
	 * 接受用户信息更新
	 * 
	 * @Author: XiaHui
	 * @Date: 2016年2月16日
	 * @ModifyUser: XiaHui
	 * @ModifyDate: 2016年2月16日
	 */
	@MethodMapping(value = "1.2.0008")
	public void updateUserStatus(@Define("userId") String userId, @Define("status") String status) {
		UserDataBox ub=appContext.getBox(UserDataBox.class);
		UserData userData = ub.getUserData(userId);
		if (null != userData) {
			userData.setStatus(status);
			EnterpriseUserService userService = appContext.getService(EnterpriseUserService.class);
			userService.updateUserData(userData);
		}
	}

	@MethodMapping(value = "1.2.0009")
	public void updateUser(@Define("userId") String userId) {
		EnterpriseUserService userService = appContext.getService(EnterpriseUserService.class);
		userService.updateUserData(userId);
	}

	/**
	 * 跟新用户在线状态
	 * @param list
	 */
	@MethodMapping(value = "1.1.0011")
	public void updateUserStatusList(@Define("list") List<HashMap<String,String>> list ) {
//		UserBox ub=appContext.getBox(UserBox.class);
//		UserData userData = ub.getUserData(userId);
//		if (null != userData) {
//			userData.setStatus(status);
//			UserService userService = appContext.getService(UserService.class);
//			userService.updateUserData(userData);
//		}
	}

	@MethodMapping(value = "0010")
	public void updateUser(@Define("userData") UserData userData) {
	}
}
