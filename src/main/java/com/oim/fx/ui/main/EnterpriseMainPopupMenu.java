package com.oim.fx.ui.main;

public class EnterpriseMainPopupMenu extends MainPopupMenu {
	
	public EnterpriseMainPopupMenu() {
		initMenu();
	}

	private void initMenu() {
		updatePasswordMenuItem.setText("修改密码");
		this.getItems().remove(updatePasswordMenuItem);
	}
}
