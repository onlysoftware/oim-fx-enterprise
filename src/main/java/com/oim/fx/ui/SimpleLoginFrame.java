/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oim.fx.ui;

/**
 * 登录窗口
 *
 * @author XiaHui
 */
public class SimpleLoginFrame extends LoginFrame {

	public SimpleLoginFrame() {
		initComponent();
		//iniEvent();
	}

	private void initComponent() {
		this.rememberCheckBox.setVisible(false);
		this.autoCheckBox.setVisible(false);
		this.registerLabel.setVisible(false);
		this.forgetLabel.setVisible(false);
		this.addAccountButton.setVisible(false);
		this.towCodeButton.setVisible(false);
	}

//	private void iniEvent() {
//
//	}
}
