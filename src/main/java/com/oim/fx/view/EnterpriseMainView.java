package com.oim.fx.view;

import java.util.List;

import com.oim.core.bean.Department;
import com.onlyxiahui.app.base.view.View;
import com.onlyxiahui.im.bean.UserData;

public interface EnterpriseMainView extends View {

	public void clearDepartmentList();
	
	public void setDepartmentList(List<Department> departmentList);
	
	public void addOrUpdateDepartment(Department department);

	public void addOrUpdateDepartmentUserData(String departmentId, UserData userData);
	
	public void updateDepartmentMemberCount(String departmentId, int totalCount, int onlineCount);

	public void addOrUpdateLastDepartment(Department department, String text);

	public void showDepartmentHeadPulse(String departmentId, boolean b);
}
