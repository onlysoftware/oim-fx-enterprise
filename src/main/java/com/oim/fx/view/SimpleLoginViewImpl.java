package com.oim.fx.view;

import com.oim.core.business.controller.PersonalController;
import com.oim.core.business.view.LoginView;
import com.oim.fx.ui.LoginFrame;
import com.oim.fx.ui.SimpleLoginFrame;
import com.onlyxiahui.app.base.AppContext;
import com.onlyxiahui.im.message.data.LoginData;

/**
 * @author: XiaHui
 * @date: 2016年10月10日 上午11:04:46
 */
public class SimpleLoginViewImpl extends LoginViewImpl implements LoginView {

	public SimpleLoginViewImpl(AppContext appContext) {
		super(appContext);
	}

	protected void login() {

		String account = loginFrame.getAccount();
		String password = loginFrame.getPassword();
		String status = loginFrame.getStatus();

		if (!loginFrame.verify()) {
			return;
		}

		LoginData loginData = new LoginData();
		loginData.setStatus(status);
		loginData.setAccount(account);
		loginData.setPassword(password);

		PersonalController pc = appContext.getController(PersonalController.class);
		pc.login(loginData);

	}

	@Override
	public LoginFrame createFrame() {
		return new SimpleLoginFrame();
	}
}
